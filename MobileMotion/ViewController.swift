//
//  ViewController.swift
//  MobileMotion
//
//  Created by Hashmi on 12/01/2022.
//

import UIKit
import CoreLocation
import AVKit

class ViewController: UIViewController {
    
    @IBOutlet weak var directionLbl: UILabel!
    @IBOutlet weak var xLbl: UILabel!
    @IBOutlet weak var yLbl: UILabel!
    @IBOutlet weak var zLbl: UILabel!
    @IBOutlet weak var mLbl: UILabel!
    
    @IBOutlet weak var startBtn: UIButton!
    @IBAction func startBtn(_ sender: Any) {

        self.beepString2 = "beep"
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
                print(self.beepString2)
                
                
                
                if self.beepString2 == "beep"{
                    self.buttonCondition()
                }else if self.beepString2 == "stopBeep"{
                    timer.invalidate()
                    self.beepString = "false"
                    self.playSound(sound: "Beeps", type: "mp3")
                    
                }else{
                    return
                }
            }
          
      
        
//        startBtn.isSelected = !startBtn.isSelected
//        if startBtn.isSelected {
//
//            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(buttonCondition), userInfo: nil, repeats: true)
//
//        }else{
//            guard self.timer != nil else {
//                return
//            }
//
//            self.timer!.invalidate()
//            self.timer = nil
//            self.playSound(sound: "Beeps", type: "mp3")
//            self.beepString = "false"
//            self.startBtn.setTitle("  Start  ", for: .normal)
//            DataHelper.cleanLoactionData()
//
//        }
//


        
    }
    
    
    
    @IBOutlet weak var stopBtn: UIButton!
    @IBAction func stopBtn(_ sender: Any) {
        self.beepString2 = "stopBeep"
        
           
    }
    
    
    
    var beepString2 = "beep"
    var beepString = ""
    var gameTimer: Timer?
    
    var locationManager: CLLocationManager?
    var audioPlayer: AVAudioPlayer?
    var player = AVAudioPlayer()
    var playingAudioPlayer: AVAudioPlayer?
    var timer : Timer?
    var xVal : String = ""
    var yVal : String = ""
    var zVal : String = ""
    var mVal : String = ""
    var deviceDir : String = ""


    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        stopBtn.isHidden = true
        stopBtn.isUserInteractionEnabled = false
        
        // Step 3: initalise and configure CLLocationManager
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.distanceFilter = 10
        locationManager?.desiredAccuracy = 5
        
        // Step 4: request authorization
        locationManager?.requestWhenInUseAuthorization()
        // or
        locationManager?.requestAlwaysAuthorization()
        locationManager?.startUpdatingHeading()
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        startBtn.layer.cornerRadius = 15
        startBtn.clipsToBounds = true
        stopBtn.layer.cornerRadius = 15
        stopBtn.clipsToBounds = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        gameTimer?.invalidate()
    }
    
    @objc func buttonCondition(){
//        startBtn.isSelected = !startBtn.isSelected
//        if startBtn.isSelected {
            
            
            // MARK: Clean data
            DataHelper.cleanLoactionData()
            
            //MARK: Save User Current location data
            DataHelper.saveLocationData(xVal: xLbl.text ?? "", yVal: yLbl.text ?? "", zVal: zLbl.text ?? "", mVal: mLbl.text ?? "", deviceDir: deviceDir ?? "")
            
            
            let obj = DataHelper.fetchLoactionData()
            let dDir = obj![0].devicedire
            print(dDir)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
                
                if self.deviceDir == "N"{
                    print(dDir, self.deviceDir)
                    if dDir != self.deviceDir{
                        self.playSound(sound: "Beeps", type: "mp3")
                        self.directionLbl.text = "User In Wrong Direction"
                        self.beepString = "true"
                        DataHelper.cleanLoactionData()
                        self.stopBtn.isHidden = false
                        self.stopBtn.isUserInteractionEnabled = true
                        
                        self.startBtn.isHidden = true
                        self.startBtn.isUserInteractionEnabled = false

                    }else{
                        self.toAndFroMotion()
                    }
                   
                }else if self.deviceDir == "NE"{
                    if dDir != self.deviceDir{
                        self.playSound(sound: "Beeps", type: "mp3")
                        self.directionLbl.text = "User In Wrong Direction"
                        self.beepString = "true"
                        DataHelper.cleanLoactionData()
                        self.stopBtn.isHidden = false
                        self.stopBtn.isUserInteractionEnabled = true
                        
                        self.startBtn.isHidden = true
                        self.startBtn.isUserInteractionEnabled = false

                    }else{
                        self.toAndFroMotion()
                    }
                }else if self.deviceDir == "E"{
                    if dDir != self.deviceDir{
                        self.playSound(sound: "Beeps", type: "mp3")
                        self.directionLbl.text = "User In Wrong Direction"
                        self.beepString = "true"
                        DataHelper.cleanLoactionData()
                        self.stopBtn.isHidden = false
                        self.stopBtn.isUserInteractionEnabled = true
                        
                        self.startBtn.isHidden = true
                        self.startBtn.isUserInteractionEnabled = false

                    }else{
                        self.toAndFroMotion()
                    }
                }else if self.deviceDir == "SE"{
                    if dDir != self.deviceDir{
                        self.playSound(sound: "Beeps", type: "mp3")
                        self.directionLbl.text = "User In Wrong Direction"
                        self.beepString = "true"
                        DataHelper.cleanLoactionData()
                        self.stopBtn.isHidden = false
                        self.stopBtn.isUserInteractionEnabled = true
                        
                        self.startBtn.isHidden = true
                        self.startBtn.isUserInteractionEnabled = false

                    }else{
                        self.toAndFroMotion()
                    }
                }else if self.deviceDir == "S"{
                    if dDir != self.deviceDir{
                        self.playSound(sound: "Beeps", type: "mp3")
                        self.directionLbl.text = "User In Wrong Direction"
                        self.beepString = "true"
                        DataHelper.cleanLoactionData()
                        self.stopBtn.isHidden = false
                        self.stopBtn.isUserInteractionEnabled = true
                        
                        self.startBtn.isHidden = true
                        self.startBtn.isUserInteractionEnabled = false
                    }else{
                        self.toAndFroMotion()
                    }
                }else if self.deviceDir == "SW"{
                    if dDir != self.deviceDir{
                        self.playSound(sound: "Beeps", type: "mp3")
                        self.directionLbl.text = "User In Wrong Direction"
                        self.beepString = "true"
                        DataHelper.cleanLoactionData()
                        self.stopBtn.isHidden = false
                        self.stopBtn.isUserInteractionEnabled = true
                        
                        self.startBtn.isHidden = true
                        self.startBtn.isUserInteractionEnabled = false

                    }else{
                        self.toAndFroMotion()
                    }
                }else if self.deviceDir == "W"{
                    if dDir != self.deviceDir{
                        self.playSound(sound: "Beeps", type: "mp3")
                        self.directionLbl.text = "User In Wrong Direction"
                        self.beepString = "true"
                        DataHelper.cleanLoactionData()
                        self.stopBtn.isHidden = false
                        self.stopBtn.isUserInteractionEnabled = true
                        
                        self.startBtn.isHidden = true
                        self.startBtn.isUserInteractionEnabled = false

                    }else{
                        self.toAndFroMotion()
                    }
                }else if self.deviceDir == "NW"{
                    if dDir != self.deviceDir{
                        self.playSound(sound: "Beeps", type: "mp3")
                        self.directionLbl.text = "User In Wrong Direction"
                        self.beepString = "true"
                        DataHelper.cleanLoactionData()
                        
                        self.stopBtn.isHidden = false
                        self.stopBtn.isUserInteractionEnabled = true
                        
                        self.startBtn.isHidden = true
                        self.startBtn.isUserInteractionEnabled = false
                    }else{
                        self.toAndFroMotion()
                    }
                }else{
                    return
                }

            })
            
//        }else{
//            self.playSound(sound: "Beeps", type: "mp3")
//            self.beepString = "false"
//            self.startBtn.setTitle("  Start  ", for: .normal)
//            DataHelper.cleanLoactionData()
//
//        }
    }
    
    
    //MARK: Check Direction if user is still
    
//    func userStill(){
//
//
//        //MARK: Fetch User Loaction Data
//        let obj = DataHelper.fetchLoactionData()
//        let xValueData = obj![0].xvalue
//        let yValueData = obj![0].yvalue
//        let zValueData = obj![0].zvalue
//        let mValueData = obj![0].mvalue
//        //MARK: Convert data into integer form
//
//
//
//        let xIntVal = Double(xValueData ?? "")
//        let yIntVal = Double(yValueData ?? "")
//        let zIntVal = Double(zValueData ?? "")
//        let mIntVal = Double(mValueData ?? "")
//
//        //MARK: IF USER IS IN STILL MOTION
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
//
//            print(xIntVal, self.xVal)
//
//            if xIntVal! <= Double(self.xVal)! + 3 || xIntVal! >= Double(self.xVal)! - 3{
//                let n = Double(self.xVal)! + 3
//                let m = Double(self.xVal)! - 3
//                print(n, m)
//                self.directionLbl.text = "User is Still"
//                self.playSound(sound: "Beeps", type: "mp3")
//                self.beepString = "true"
//
//
//
////                //MARK: Clean User Current location data
////                DataHelper.cleanLoactionData()
//
//            }else if yIntVal! <= Double(self.yVal)! + 3 || yIntVal! >= Double(self.yVal)! - 3{
//
//                let n = Double(self.yVal)! + 3
//                let m = Double(self.yVal)! - 3
//                print(n, m)
//
//                self.directionLbl.text = "User is Still"
//                self.playSound(sound: "Beeps", type: "mp3")
//                self.beepString = "true"
//                //MARK: Save User Current location data
//                DataHelper.saveLocationData(xVal: self.xLbl.text ?? "", yVal: self.yLbl.text ?? "", zVal: self.zLbl.text ?? "", mVal: self.mLbl.text ?? "", deviceDir: self.deviceDir ?? "")
////                //MARK: Clean User Current location data
////                DataHelper.cleanLoactionData()
//
//            }else if zIntVal! <= Double(self.zVal)! + 3 || zIntVal! >= Double(self.zVal)! - 3{
//
//                let n = Double(self.zVal)! + 3
//                let m = Double(self.zVal)! - 3
//                print(n, m)
//
//                self.directionLbl.text = "User is Still"
//                self.playSound(sound: "Beeps", type: "mp3")
//                self.beepString = "true"
//                //MARK: Save User Current location data
//                DataHelper.saveLocationData(xVal: self.xLbl.text ?? "", yVal: self.yLbl.text ?? "", zVal: self.zLbl.text ?? "", mVal: self.mLbl.text ?? "", deviceDir: self.deviceDir ?? "")
////
////                //MARK: Clean User Current location data
////                DataHelper.cleanLoactionData()
//
//            }else if mIntVal! <= Double(self.mVal)! + 3 || mIntVal! >= Double(self.mVal)! - 3{
//                self.directionLbl.text = "User is Still"
//                self.playSound(sound: "Beeps", type: "mp3")
//                self.beepString = "true"
//
//                //MARK: Save User Current location data
//                DataHelper.saveLocationData(xVal: self.xLbl.text ?? "", yVal: self.yLbl.text ?? "", zVal: self.zLbl.text ?? "", mVal: self.mLbl.text ?? "", deviceDir: self.deviceDir ?? "")
//
////                //MARK: Clean User Current location data
////                DataHelper.cleanLoactionData()
//
//            }else if xIntVal! <= Double(self.xVal)! + 3 || xIntVal! >= Double(self.xVal)! - 3{
//
//                self.directionLbl.text = "User is Still"
//                self.playSound(sound: "Beeps", type: "mp3")
//                self.beepString = "true"
//
////                //MARK: Clean User Current location data
////                DataHelper.cleanLoactionData()
//
//            }else if yIntVal! <= Double(self.yVal)! + 3 || yIntVal! >= Double(self.yVal)! - 3{
//
//                self.directionLbl.text = "User is Still"
//                self.playSound(sound: "Beeps", type: "mp3")
//                self.beepString = "true"
//
//                //MARK: Clean User Current location data
//                DataHelper.cleanLoactionData()
//
//            }else if zIntVal! <= Double(self.zVal)! + 3 || zIntVal! >= Double(self.zVal)! - 3{
//
//                self.directionLbl.text = "User is Still"
//                self.playSound(sound: "Beeps", type: "mp3")
//                self.beepString = "true"
//
//                //MARK: Clean User Current location data
//                DataHelper.cleanLoactionData()
//
//            }else if mIntVal! <= Double(self.mVal)! + 3 || mIntVal! >= Double(self.mVal)! - 3{
//
//                self.directionLbl.text = "User is Still"
//                self.playSound(sound: "Beeps", type: "mp3")
//                self.beepString = "true"
//
//                //MARK: Clean User Current location data
//                DataHelper.cleanLoactionData()
//
//            }else{
//                print(self.xVal)
//                self.directionLbl.text = "User in Forward"
//                self.toAndFroMotion()
//                DataHelper.cleanLoactionData()
//            }
//        })
//    }
    
    //MARK: Forwrd and backword
    
    func toAndFroMotion(){
        
        
        //MARK: Save User Current location data
        DataHelper.saveLocationData(xVal: xLbl.text ?? "", yVal: yLbl.text ?? "", zVal: zLbl.text ?? "", mVal: mLbl.text ?? "", deviceDir: deviceDir ?? "")
        
        
        //MARK: Fetch User Loaction Data
        let obj = DataHelper.fetchLoactionData()
        let xValueData = obj![0].xvalue
        let yValueData = obj![0].yvalue
        let zValueData = obj![0].zvalue
        let mValueData = obj![0].mvalue
        //MARK: Convert data into integer form
        
        
        
        let xIntVal = Double(xValueData ?? "")
        let yIntVal = Double(yValueData ?? "")
        let zIntVal = Double(zValueData ?? "")
        let mIntVal = Double(mValueData ?? "")
        
        if xIntVal! > Double(xVal)! && yIntVal! < Double(yVal)!{
            self.directionLbl.text = "User In Forward Direction"
            self.playSound(sound: "Beeps", type: "mp3")
            self.beepString = "true"
            self.buttonCondition()
            self.stopBtn.isHidden = false
            self.stopBtn.isUserInteractionEnabled = true
            
            self.startBtn.isHidden = true
            self.startBtn.isUserInteractionEnabled = false
//            //MARK: Save User Current location data
//            DataHelper.saveLocationData(xVal: self.xLbl.text ?? "", yVal: self.yLbl.text ?? "", zVal: self.zLbl.text ?? "", mVal: self.mLbl.text ?? "", deviceDir: self.deviceDir ?? "")
           
            //MARK: Clean User data
            DataHelper.cleanLoactionData()
            
        }else if xIntVal! < Double(xVal)! && yIntVal! > Double(yVal)!{
            self.directionLbl.text = "User is Backword Direction"
            self.playSound(sound: "Beeps", type: "mp3")
            self.beepString = "true"
            self.stopBtn.isHidden = false
            self.stopBtn.isUserInteractionEnabled = true
            
            self.startBtn.isHidden = true
            self.startBtn.isUserInteractionEnabled = false
            //MARK: Clean User Current location data
            DataHelper.cleanLoactionData()
            return
        }else if xIntVal! <= Double(self.xVal)! + 2 || xIntVal! >= Double(self.xVal)! - 2{
            self.directionLbl.text = "User is Still"
            self.playSound(sound: "Beeps", type: "mp3")
            self.beepString = "true"
            self.stopBtn.isHidden = false
            self.stopBtn.isUserInteractionEnabled = true
            
            self.startBtn.isHidden = true
            self.startBtn.isUserInteractionEnabled = false
            //MARK: Save User Current location data
           // DataHelper.saveLocationData(xVal: self.xLbl.text ?? "", yVal: self.yLbl.text ?? "", zVal: self.zLbl.text ?? "", mVal: self.mLbl.text ?? "", deviceDir: self.deviceDir ?? "")
            
            //MARK: Clean User Current location data
            DataHelper.cleanLoactionData()
        }else{
            return
        }
        
    }
    
    
    func playSound(sound: String, type: String) {
        if let path = Bundle.main.path(forResource: sound, ofType: type) {
            do {
                try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [.mixWithOthers])
                try AVAudioSession.sharedInstance().setActive(true)

                audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
                //audioPlayer?.play()
                self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
                    
                    print(self.beepString)
                    
                    if self.beepString == "true"{
                        self.audioPlayer?.play()
                    }else{
                        timer.invalidate()
                        self.audioPlayer?.stop()
                        self.beepString2 = "stopBeep"
                        
                        self.stopBtn.isHidden = true
                        self.stopBtn.isUserInteractionEnabled = false
                        
                        self.startBtn.isHidden = false
                        self.startBtn.isUserInteractionEnabled = true
                    }
                    
                    
                        })
            } catch {
                print("ERROR")
            }
        }
    }

    
      
}
// Step 5: Implement the CLLocationManagerDelegate to handle the callback from CLLocationManager
extension ViewController: CLLocationManagerDelegate {
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    switch status {
    case .denied: // Setting option: Never
      print("LocationManager didChangeAuthorization denied")
    case .notDetermined: // Setting option: Ask Next Time
      print("LocationManager didChangeAuthorization notDetermined")
    case .authorizedWhenInUse: // Setting option: While Using the App
      print("LocationManager didChangeAuthorization authorizedWhenInUse")
      
      // Stpe 6: Request a one-time location information
      locationManager?.requestLocation()
    case .authorizedAlways: // Setting option: Always
      print("LocationManager didChangeAuthorization authorizedAlways")
      
      // Stpe 6: Request a one-time location information
      locationManager?.requestLocation()
    case .restricted: // Restricted by parental control
      print("LocationManager didChangeAuthorization restricted")
    default:
      print("LocationManager didChangeAuthorization")
    }
  }

    // Step 7: Handle the location information
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
      print("LocationManager didUpdateLocations: numberOfLocation: \(locations.count)")
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        print(locations)
      
      locations.forEach { (location) in
          print(location)
        print("LocationManager didUpdateLocations: \(dateFormatter.string(from: location.timestamp)); \(location.coordinate.latitude), \(location.coordinate.longitude)")
        print("LocationManager altitude: \(location.altitude)")
        print("LocationManager floor?.level: \(location.floor?.level)")
        print("LocationManager horizontalAccuracy: \(location.horizontalAccuracy)")
        print("LocationManager verticalAccuracy: \(location.verticalAccuracy)")
        print("LocationManager speedAccuracy: \(location.speedAccuracy)")
        print("LocationManager speed: \(location.speed)")
        print("LocationManager timestamp: \(location.timestamp)")
        print("LocationManager courseAccuracy: \(location.courseAccuracy)") // 13.4
        print("LocationManager course: \(location.course)")
      }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        print(newHeading.x)
        print(newHeading.y)
        print(newHeading.z)
        print(newHeading.magneticHeading)
        
        xVal = "\(newHeading.x)"
        yVal = "\(newHeading.y)"
        zVal = "\(newHeading.z)"
        mVal = "\(newHeading.magneticHeading)"
        
      
        
        xLbl.text = "\(newHeading.x)"
        yLbl.text = "\(newHeading.y)"
        zLbl.text = "\(newHeading.z)"
        mLbl.text = "\(newHeading.magneticHeading)"
        
        var h = newHeading.magneticHeading
           let h2 = newHeading.trueHeading // will be -1 if we have no location info
           print("\(h) \(h2) ")
           if h2 >= 0 {
               h = h2
           }
           let cards = ["N", "NE", "E", "SE", "S", "SW", "W", "NW"]
           var dir = "N"
           for (ix, card) in cards.enumerated() {
               if h < 45.0/2.0 + 45.0*Double(ix) {
                   dir = card
                   break
               }
           }
           print(dir)
        deviceDir = dir
       
        
        
       
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
      print("LocationManager didFailWithError \(error.localizedDescription)")
      if let error = error as? CLError, error.code == .denied {
         // Location updates are not authorized.
        // To prevent forever looping of `didFailWithError` callback
         locationManager?.stopMonitoringSignificantLocationChanges()
         return
      }
    }
    
 
  }
