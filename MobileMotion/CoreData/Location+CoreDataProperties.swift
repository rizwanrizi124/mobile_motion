//
//  Location+CoreDataProperties.swift
//  MobileMotion
//
//  Created by Hashmi on 15/01/2022.
//
//

import Foundation
import CoreData


extension Location {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Location> {
        return NSFetchRequest<Location>(entityName: "Location")
    }

    @NSManaged public var xvalue: String?
    @NSManaged public var yvalue: String?
    @NSManaged public var zvalue: String?
    @NSManaged public var mvalue: String?
    @NSManaged public var devicedire: String?

}

extension Location : Identifiable {

}
