//
//  DataHelper.swift
//  MobileMotion
//
//  Created by Hashmi on 15/01/2022.
//

import Foundation
import CoreData
import UIKit

class DataHelper{
    //MARK: - Video Part 1
    private class func getContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        return appDelegate.persistentContainer.viewContext
    }
    
    class func saveLocationData(xVal: String, yVal: String, zVal: String, mVal: String, deviceDir: String) -> Bool {
        let context = getContext()
        let entity = NSEntityDescription.entity(forEntityName: "Location", in: context)
        let manageObject = NSManagedObject(entity: entity!, insertInto: context)
        
        manageObject.setValue(xVal, forKey: "xvalue")
        manageObject.setValue(yVal, forKey: "yvalue")
        manageObject.setValue(zVal, forKey: "zvalue")
        manageObject.setValue(mVal, forKey: "mvalue")
        manageObject.setValue(deviceDir, forKey: "devicedire")
        
        do {
            try context.save()
            return true
        }catch {
            return false
        }
    }
    
    
    
    //MARK: Fetch Data
    
    
    class func fetchLoactionData() -> [Location]? {
        let context = getContext()
        var currency:[Location]? = nil
        do {
            currency = try context.fetch(Location.fetchRequest())
            return currency
        }catch {
            return currency
        }
    }
    
   
    
    //MARK: - Delete
    class func deleteUserInfo(userInfo : Location) -> Bool {
    
        let context = getContext()
        context.delete(userInfo)
        
        do {
            try context.save()
            return true
        }catch {
            return false
        }
        
    }
    

    
    class func cleanLoactionData() -> Bool {
        let context = getContext()
        let delete = NSBatchDeleteRequest(fetchRequest: Location.fetchRequest())
        
        do {
            try context.execute(delete)
            return true
        }catch {
            return false
        }
    }
   
    
    //MARK: - Video Part 3
//    class func filterData() -> [CurrencyDataCore]? {
//        let context = getContext()
//        let fetchRequest:NSFetchRequest<CurrencyDataCore> = CurrencyDataCore.fetchRequest()
//        var user:[CurrencyDataCore]? = nil
//
//        let predicate = NSPredicate(format: "password contains[c] %@", "2")
//        fetchRequest.predicate = predicate
//
//        do {
//            user = try context.fetch(fetchRequest)
//            return user
//
//        }catch {
//            return user
//        }
//    }
}
